# RD/GMA2Tools Support

Welcome to the support page for RD/GMA2Tools, the Extension Toolkit for MA Lighting GrandMA2 consoles and OnPC.

If you are new to RD/GMA2Tools, I recommend that you head to the [official webpage](http://ricardo-dias.com/gma2tools/) to get you up and running.

In the [Repository](https://bitbucket.org/ricdays/gma2tools-support/src), you will find mapping files for MIDI devices.

In [Downloads](https://bitbucket.org/ricdays/gma2tools-support/downloads), you can download all files at once.

There's also a [community Facebook group](https://www.facebook.com/groups/gma2tools/) where you can ask for help, share ideas, give feedback, etc.